# RangeList

RangeList is a library for fast and efficient processing of intervals.

A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201).

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add range_list_rb

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install range_list_rb

## Usage

```ruby
range_list = RangeList.new # create a RangeList instance

range_list.add([1, 5])
range_list.print # [1, 5)

range_list.add([10, 20])
range_list.print # [1, 5) [10, 20)

range_list.add([20, 20])
range_list.print # [1, 5) [10, 20)

range_list.remove([3, 12])
range_list.print # [1, 3) [12, 20)

range_list.contains?(1) # true
range_list.contains?(2) # true
range_list.contains?(3) # false
range_list.contains?(20) # false
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/songzheng1997/range_list. This project is intended to be a safe, welcoming space for collaboration.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
