# frozen_string_literal: true

require_relative "range_list/version"
require_relative "range_list/map/tree_map"
require_relative "range_list/errors"

# range list
class RangeList
  include Enumerable

  def initialize
    @tree_map = Map::TreeMap.new
  end

  # Add specified range into current RangeList
  # @param range [Array<Integer>] Two integer element such as [left, right], include left, exclude right.
  # @return [RangeList]
  # @raise [IllegalArgumentError] when range is invalid
  def add(range)
    validate_range!(range)
    self if validate_empty_range?(range)

    # 1. Compute the range of the merged interval
    left_bound = range[0]
    left_floor_range = @tree_map.floor_entry(range[0])
    if !left_floor_range.nil? && left_floor_range[1] >= range[0]
      # left_floor_range and range has overlapping interval
      left_bound = left_floor_range[0]
    end

    right_bound = range[1]
    right_floor_range = @tree_map.floor_entry(range[1])
    if !right_floor_range.nil? && right_floor_range[1] >= range[1]
      # right_floor_range and range has overlapping interval
      right_bound = right_floor_range[1]
    end

    # 2. Remove overlapping intervals
    overlapping_ranges = @tree_map.sub_map(left_bound, right_bound)
    overlapping_ranges.each { |left, _right| @tree_map.remove(left) }

    # 3. Insert new range
    @tree_map.put(left_bound, right_bound)

    self
  end

  # Remove specified range from current range
  # @param range [Array<Integer>] Two integer element such as [left, right], include left, exclude right.
  # @return [RangeList]
  # @raise [IllegalArgumentError] when range is invalid
  def remove(range)
    validate_range!(range)
    self if validate_empty_range?(range)

    # 1. Remove overlapping intervals
    right_lower_range = @tree_map.floor_entry(range[1] - 1)
    if !right_lower_range.nil? && right_lower_range[1] > range[1]
      # right_lower_range and range has overlapping interval
      # insert new range after remove overlapping interval
      @tree_map.put(range[1], right_lower_range[1])
    end

    left_lower_range = @tree_map.floor_entry(range[0] - 1)
    if !left_lower_range.nil? && left_lower_range[1] > range[0]
      # left_lower_range and range has overlapping interval
      # insert new range after remove overlapping interval
      @tree_map.put(left_lower_range[0], range[0])
    end

    # 2. Remove ranges completely covered by input range
    # to_key is range[1] - 1 because right bound is exclusive
    overlapping_ranges = @tree_map.sub_map(range[0], range[1] - 1)
    overlapping_ranges.each { |left, _right| @tree_map.remove(left) }

    self
  end

  # Print RangeList to stdout
  def print
    Kernel.print to_s
  end

  # Returns true if range list contains input element, or false if not contains.
  # @param element [Integer]
  # @return [Boolean]
  # @raise [IllegalArgumentError] when element is invalid
  def contains?(element)
    validate_element!(element)

    floor_range = @tree_map.floor_entry(element)
    !floor_range.nil? && floor_range[1] > element
  end

  def each(&block)
    @tree_map.each(&block)
  end

  def to_s
    map { |k, v| "[#{k}, #{v})" }.join(" ")
  end

  private

  attr_reader :tree_map

  def validate_range!(range)
    raise IllegalArgumentError, "range cannot be nil" if range.nil?
    raise IllegalArgumentError, "range must be Array type" unless range.is_a?(Array)
    unless range.size == 2 && range.all? { |elem| elem.is_a?(Integer) }
      raise IllegalArgumentError, "range can only contain two Integer elements"
    end
    raise IllegalArgumentError, "range's left element must less equal than right element" unless range[0] <= range[1]
  end

  def validate_empty_range?(range)
    range[0] >= range[1]
  end

  def validate_element!(element)
    raise IllegalArgumentError, "element cannot be nil" if element.nil?
    raise IllegalArgumentError, "element must be Integer type" unless element.is_a?(Integer)
  end
end
