# frozen_string_literal: true

# errors in RangeList
class RangeList
  # base error define
  class Error < StandardError; end

  # raised when input arguments are illegal
  class IllegalArgumentError < Error; end
end
