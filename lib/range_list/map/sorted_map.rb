# frozen_string_literal: true

class RangeList
  module Map
    # A Map that provides a ordering on its keys.
    module SortedMap
      # Returns the value to which the specified key is mapped, or nil if this map contains no mapping for the key.
      def get(key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      # Associates the specified value with the specified key in this map.
      # If the map previously contained a mapping for the key, the old value is replaced by the specified value.
      def put(key, value)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      # Removes the mapping for a key from this map if it is present.
      def remove(key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      # Returns a key-value mapping associated with the greatest key less than or equal to the given key,
      # or null if there is no such key.
      def floor_entry(key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      # Returns a key-value mapping associated with the least key greater than or equal to the given key,
      # or null if there is no such key.
      def ceiling_entry(key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      # Returns a key-value mapping associated with the greatest key strictly less than the given key,
      # or null if there is no such key.
      def lower_entry(key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      # Returns a key-value mapping associated with the least key strictly greater than the given key,
      # or null if there is no such key.
      def higher_entry(key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      # Returns a view of the portion of this map whose keys range from from_key, inclusive, to to_key, exclusive.
      def sub_map(from_key, to_key)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end

      # Support iteration.
      def each(&block)
        raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
      end
    end
  end
end
