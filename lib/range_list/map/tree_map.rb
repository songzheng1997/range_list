# frozen_string_literal: true

require "rbtree"
require_relative "sorted_map"

class RangeList
  module Map
    # Sorted map implementation using red-black tree
    class TreeMap
      include SortedMap

      def initialize
        @rbtree = RBTree.new
      end

      def get(key)
        @rbtree[key]
      end

      def put(key, value)
        @rbtree[key] = value
      end

      def remove(key)
        @rbtree.delete(key)
      end

      def floor_entry(key)
        @rbtree.upper_bound(key)
      end

      def ceiling_entry(key)
        @rbtree.lower_bound(key)
      end

      def sub_map(from_key, to_key)
        @rbtree.bound(from_key, to_key).to_a
      end

      def each(&block)
        @rbtree.each(&block)
      end

      private

      attr_reader :rbtree
    end
  end
end
