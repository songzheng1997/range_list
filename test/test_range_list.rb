# frozen_string_literal: true

require "test_helper"

class TestRangeList < Minitest::Test
  def setup
    @range_list = RangeList.new
  end

  def test_valid_version
    refute_nil ::RangeList::VERSION
  end

  def test_range_list
    @range_list.add([1, 5])
    assert_output("[1, 5)") { @range_list.print }

    @range_list.add([10, 20])
    assert_output("[1, 5) [10, 20)") { @range_list.print }

    @range_list.add([20, 20])
    assert_output("[1, 5) [10, 20)") { @range_list.print }

    @range_list.add([20, 21])
    assert_output("[1, 5) [10, 21)") { @range_list.print }

    @range_list.add([2, 4])
    assert_output("[1, 5) [10, 21)") { @range_list.print }

    @range_list.add([3, 8])
    assert_output("[1, 8) [10, 21)") { @range_list.print }

    @range_list.remove([10, 10])
    assert_output("[1, 8) [10, 21)") { @range_list.print }

    @range_list.remove([10, 11])
    assert_output("[1, 8) [11, 21)") { @range_list.print }

    @range_list.remove([15, 17])
    assert_output("[1, 8) [11, 15) [17, 21)") { @range_list.print }

    @range_list.remove([3, 19])
    assert_output("[1, 3) [19, 21)") { @range_list.print }

    # contains?
    assert @range_list.contains?(1)
    assert @range_list.contains?(2)
    assert !@range_list.contains?(3)
    assert !@range_list.contains?(21)
  end

  def test_add_invalid_argument
    assert_raises(RangeList::IllegalArgumentError) { @range_list.add("") }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.add(%w[1 2]) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.add([1, "2"]) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.add([2, 1]) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.add([1]) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.add([]) }
  end

  def test_remove_invalid_argument
    assert_raises(RangeList::IllegalArgumentError) { @range_list.remove("") }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.remove(%w[1 2]) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.remove([1, "2"]) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.remove([2, 1]) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.remove([1]) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.remove([]) }
  end

  def test_contains_invalid_argument
    assert_raises(RangeList::IllegalArgumentError) { @range_list.contains?(nil) }
    assert_raises(RangeList::IllegalArgumentError) { @range_list.contains?("") }
  end
end
